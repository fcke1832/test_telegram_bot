import requests
import telebot
import time

from bs4 import BeautifulSoup
from datetime import datetime

tg_token = '934667817:AAEtxnwMNzPWatzAI3N_tyaujy_WeZWLetk'
bot = telebot.TeleBot(tg_token)
users_data = {}

zodiacs_list = [
    (120, 'Козерог', 'capricorn'), (218, 'Водолей', 'aquarius'), (320, 'Рыбы', 'pisces'),
    (420, 'Овен', 'aries'), (521, 'Телец', 'taurus'), (621, 'Близнецы', 'gemini'),
    (722, 'Рак', 'cancer'), (823, 'Лев', 'leo'), (923, 'Дева', 'virgo'),
    (1023, 'Весы', 'libra'), (1122, 'Скорпион', 'scorpio'), (1222, 'Стрелец', 'sagittarius'),
    (1231, 'Козерог', 'capricorn')
]


@bot.message_handler(commands=['help'])
def help_message(message):
    bot.send_message(
        message.chat.id,
        'Это бот-гадалка, который угадает твоё имя и возраст, расскажет гороскоп на сегодня. Чтобы начать диалог - набери команду "/start". Чтобы получить гороскоп на сегодня - набери "/date"'
    )


@bot.message_handler(commands=['date'])
def date_message(message):
    msg = bot.send_message(message.chat.id, 'Введите дату своего дня рождения в формате dd.mm.YYYY')
    bot.register_next_step_handler(msg, get_horoscope)


def get_zodiac(date_number):
    for zodiac in zodiacs_list:
        if date_number <= zodiac[0]:
            return zodiac[1], zodiac[2]


def get_horoscope(message):
    date_str = message.text
    try:
        date_object = datetime.strptime(date_str, '%d.%m.%Y').date()
    except:
        bot.send_message(message.chat.id, 'Некорректная дата')
        return
    date_number = int("".join((str(date_object.month), '%02d' % date_object.day)))
    zodiac, part_url = get_zodiac(date_number)

    url = 'http://1001goroskop.ru/?znak={}'.format(part_url)
    r = requests.get(url)
    if r.status_code != 200:
        bot.send_message(message.chat.id, 'Что-то пошло не так...')
        return
    soup = BeautifulSoup(r.text, 'lxml')
    horoscope_today = soup.find('div', {'itemprop': 'description'}).text
    bot.send_message(message.chat.id, 'Ты {}'.format(zodiac))
    bot.send_message(message.chat.id, horoscope_today)


@bot.message_handler(commands=['start'])
def start_message(message):
    msg = bot.send_message(message.chat.id, 'Это кто тут такой дерзкий? Скажи своё имя?')
    users_data[message.chat.id] = {}
    bot.register_next_step_handler(msg, get_name)


def get_name(message):
    users_data[message.chat.id].update(dict(firstname=message.text))
    msg = bot.send_message(message.chat.id, 'Какая у тебя фамилия?')
    # print(msg)
    bot.register_next_step_handler(msg, get_secondname)


def get_secondname(message):
    users_data[message.chat.id].update(dict(secondname=message.text))
    bot.send_message(message.chat.id, 'Номер СНИЛС?')
    time.sleep(1)
    msg = bot.send_message(message.chat.id, 'Это шутка. Сколько тебе лет?')
    bot.register_next_step_handler(msg, get_age)


def get_age(message):
    age = message.text
    if not age.isdigit():
        msg = bot.send_message(message.chat.id, 'А в цифрах это сколько?')
        bot.register_next_step_handler(msg, get_age)
        return

    users_data[message.chat.id].update(dict(age=age))
    firstname = users_data[message.chat.id]['firstname']
    secondname = users_data[message.chat.id]['secondname']
    age = users_data[message.chat.id]['age']
    result = 'Ты {firstname} {secondname}, тебе {age} лет'.format(
        firstname=firstname,
        secondname=secondname,
        age=age,
    )
    bot.send_message(message.chat.id, result)

bot.polling(none_stop=True)
